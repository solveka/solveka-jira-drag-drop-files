package tr.com.solveka.jira.sjddf.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import webwork.config.Configuration;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
/**
 * URLS must follow this pattern
 * create  POST /urlOfRestResource
 * read  GET /urlOfRestResource/path
 * update  PUT /urlOfRestResource/id
 * delete  DELETE /urlOfRestResource/id
 */
@Path ("/applicationProps")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class ApplicationPropertiesResource
{
	private Logger log = LoggerFactory.getLogger(ApplicationPropertiesResource.class);
	CacheControl cacheControl;
	
	public ApplicationPropertiesResource() {
        CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
    }
 
    @GET
    @Path ("/maxFileSize")
	public Response getMaxPermittedFileSize() {
    	String maxSize = Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE);
    	
    	if (maxSize == null || maxSize.length() == 0) {
    		maxSize = "10485760";
    		log.debug("Maximum attachment size property in null. It is set to " + maxSize);
    	}
    	return Response.ok(maxSize).cacheControl(cacheControl).build();
    }
}